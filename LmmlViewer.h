#ifndef LmmlViewer_H
#define LmmlViewer_H

#define TEST
//#define UBUNTU_OR_MAC

#include "build/ui_LmmlViewer.h"

#include <QMainWindow>

#include <vtkSmartPointer.h>
#include <vtkObject.h>
#include <string>
#include <vtkRenderer.h>
class vtkEventQtSlotConnect;

class LmmlViewer : public QMainWindow, private Ui::LmmlViewer
{
	Q_OBJECT
public:
	vtkRenderer *renderer;
	vtkGenericOpenGLRenderWindow *renderWindow;

	LmmlViewer();
	vtkRenderer *GetRenderer();
	vtkGenericOpenGLRenderWindow *GetRenderWindow();

public slots:

	void slot_clicked(vtkObject *, unsigned long, void *, void *);
	// open Image file
	std::string slotOpenImageryFile();
	// open JSON file
	std::string slotOpenJSONFile();
	void slotSaveJSON();
	void slotSaveJSON(std::string activeProfileText, std::string jsonFileName);
	void startRequest(const QUrl &requestedUrl);
	void httpReadyRead();
	void httpFinished();
	// Language
	void slotEnglish();
	void slotJapanese();
	// sliders
	std::map<int, double> initialiseSliders();
	std::map<int, double> sliderCallBack();
	void UpdateRenderer(std::map<int, double> _changedSlider);
	// injuries
	void injuriesListCallBack(std::string newInjury);
	// exit
	void slotExit();

private:
	vtkSmartPointer<vtkEventQtSlotConnect> Connections;
	std::string m_textJson;
	std::map<int, double> m_sliders;

};

#endif

