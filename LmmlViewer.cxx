#include "LmmlViewer.h"

#include "vtkGenericOpenGLRenderWindow.h"
#include <vtkNew.h>
#include <vtkPolyDataMapper.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkSphereSource.h>
#include <vtkEventQtSlotConnect.h>
#include <vtkInteractorStyleTrackballActor.h>
#include <vtkFixedPointVolumeRayCastMapper.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkNamedColors.h>
#include <vtkMetaImageReader.h>
#include <vtkPiecewiseFunction.h>
#include <vtkVolumeProperty.h>
#include <vtkCamera.h>
#include <vtkColorTransferFunction.h>

#ifdef UBUNTU_OR_MAC
#include "vtkAutoInit.h"
VTK_MODULE_INIT(vtkRenderingOpenGL2);
VTK_MODULE_INIT(vtkInteractionStyle);
VTK_MODULE_INIT(vtkRenderingVolumeOpenGL2);
#endif

#include <string>
#include <array>

using namespace std;

// Constructor
LmmlViewer::LmmlViewer()
{

	this->setupUi(this);
	vtkSmartPointer<vtkGenericOpenGLRenderWindow> _renderWindow = vtkSmartPointer<vtkGenericOpenGLRenderWindow>::New();
	this->renderWindow = _renderWindow;
	this->qvtkWidget->SetRenderWindow(_renderWindow);
	vtkSmartPointer<vtkRenderer> _renderer = vtkSmartPointer<vtkRenderer>::New();

	vtkSmartPointer<vtkNamedColors> colors =
		vtkSmartPointer<vtkNamedColors>::New();

	std::array<unsigned char, 4> bkg{ {230, 230, 230, 230} };
	colors->SetColor("BkgColor", bkg.data());
	_renderer->SetBackground(colors->GetColor3d("BkgColor").GetData());

	this->renderer = _renderer;

	vtkNew<vtkEventQtSlotConnect> slotConnector;
	this->Connections = slotConnector;

	cout << "DEBUG 0\n";

	//////////////////////////////////////
	// Set up
	string CT_FileName = "../Data/FullHead.mhd";

	vtkSmartPointer<vtkFixedPointVolumeRayCastMapper> volumeMapper =
		vtkSmartPointer<vtkFixedPointVolumeRayCastMapper>::New();
	vtkSmartPointer<vtkInteractorStyleTrackballCamera> interactor =
		vtkSmartPointer<vtkInteractorStyleTrackballCamera>::New();

	vtkSmartPointer<vtkRenderWindowInteractor> iren = vtkSmartPointer<vtkRenderWindowInteractor>::New();
	//iren->SetInteractorStyle(interactor);
	//iren->SetRenderWindow(_renderWindow);
	//interactor->SetCurrentRenderer(_renderer);

	// cout << "DEBUG 0\n";

	// Volume reader :
	vtkSmartPointer<vtkMetaImageReader> reader =
		vtkSmartPointer<vtkMetaImageReader>::New();
	const char *CT_cstr = CT_FileName.c_str();
	//cout << CT_cstr << endl;

	reader->SetFileName(CT_cstr);
	reader->Update();

	// The volume will be displayed by ray-cast alpha compositing.
	volumeMapper->SetInputConnection(reader->GetOutputPort());

	vtkSmartPointer<vtkPiecewiseFunction> volumeGradientOpacity =
		vtkSmartPointer<vtkPiecewiseFunction>::New();
	volumeGradientOpacity->AddPoint(0, 0.0);
	volumeGradientOpacity->AddPoint(50, 0.2);
	volumeGradientOpacity->AddPoint(100, 1.0);

	vtkSmartPointer<vtkPiecewiseFunction> volumeScalarOpacity =
		vtkSmartPointer<vtkPiecewiseFunction>::New();
	volumeScalarOpacity->AddPoint(500, 1);

	vtkSmartPointer<vtkColorTransferFunction> volumeColor =
		vtkSmartPointer<vtkColorTransferFunction>::New();
	volumeColor->AddRGBPoint(0, 0.0, 0.0, 0.0);
	volumeColor->AddRGBPoint(500, 1.0, 0.5, 0.3);
	volumeColor->AddRGBPoint(1000, 1.0, 0.5, 0.3);
	volumeColor->AddRGBPoint(1150, 1.0, 1.0, 0.9);

	vtkSmartPointer<vtkVolumeProperty> volumeProperty =
		vtkSmartPointer<vtkVolumeProperty>::New();
	volumeProperty->SetColor(volumeColor);
	volumeProperty->SetScalarOpacity(volumeScalarOpacity);
	volumeProperty->SetGradientOpacity(volumeGradientOpacity);
	volumeProperty->ShadeOn();
	volumeProperty->SetAmbient(0.4);
	volumeProperty->SetDiffuse(0.6);
	volumeProperty->SetSpecular(0.5);

	vtkSmartPointer<vtkVolume> volume =
		vtkSmartPointer<vtkVolume>::New();
	volume->SetMapper(volumeMapper);
	volume->SetProperty(volumeProperty);

	_renderer->AddViewProp(volume);

	// Camera
	vtkSmartPointer<vtkCamera> camera = _renderer->GetActiveCamera();
	camera->SetViewUp(0, 0, -1);
	camera->SetPosition(0, -400, 0);
	camera->SetFocalPoint(0, 0, 0);
	camera->Elevation(30.0);

	//iren->Start();

	////////////////////////////////////////

	this->qvtkWidget->GetRenderWindow()->AddRenderer(_renderer);
	cout << "DEBUG 0\n";


	//This is called everytime the renderer is clicked with left mouse button
	this->Connections->Connect(this->qvtkWidget->GetRenderWindow()->GetInteractor(),
		vtkCommand::LeftButtonPressEvent,
		this,
		SLOT(slot_clicked(vtkObject *, unsigned long, void *, void *)));

	cout << "DEBUG 0\n";

};

void LmmlViewer::slot_clicked(vtkObject *, unsigned long, void *, void *)
{
	std::cout << "Clicked." << std::endl;
}

vtkRenderer *LmmlViewer::GetRenderer()
{
	return this->renderer;
}

vtkGenericOpenGLRenderWindow *LmmlViewer::GetRenderWindow()
{
	return NULL;
}

std::string LmmlViewer::slotOpenImageryFile()
{
	QDir dir;
	QString currentPath = dir.currentPath();

	// open file
	QString filePathName = QFileDialog::getOpenFileName(
		this,
		tr("Open File"),
		currentPath,
		tr("Images (*.mhd *.raw *.dicom)"));

	QByteArray byteArray = filePathName.toLocal8Bit();
	std::string path = byteArray.data();

#ifdef TEST
	std::cout << path.c_str() << std::endl;
#endif

	return path;
}

std::string LmmlViewer::slotOpenJSONFile()
{
	std::string emptySt = "";

	std::string urlSt = "http://localhost:8000/";
	const QString urlSpec = urlSt.c_str();
	if (urlSpec.isEmpty())
		return emptySt;

	const QUrl newUrl = QUrl::fromUserInput(urlSpec);
	if (!newUrl.isValid()) {
		QMessageBox::information(this, tr("Error"), tr("Invalid URL: %1: %2").arg(urlSpec, newUrl.errorString()));
		return emptySt;
	}

	startRequest(newUrl);

	eventLoop.exec();
	std::string textJson = m_textJson;
	m_textJson.clear();

#ifdef TEST
	std::cout << textJson.c_str() << std::endl;
#endif

	return textJson;
}

void LmmlViewer::startRequest(const QUrl &requestedUrl)
{
	url = requestedUrl;
	reply = manager->get(QNetworkRequest(url));

	connect(reply, &QNetworkReply::finished, this, &LmmlViewer::httpFinished);
	connect(reply, &QIODevice::readyRead, this, &LmmlViewer::httpReadyRead);
}

void LmmlViewer::httpFinished()
{
	QFileInfo fi;
	if (reply->error()) {
		QFile::remove(fi.absoluteFilePath());
		reply->deleteLater();
		reply = nullptr;
		return;
	}

	const QVariant redirectionTarget = reply->attribute(QNetworkRequest::RedirectionTargetAttribute);

	reply->deleteLater();
	reply = nullptr;

	if (!redirectionTarget.isNull()) {
		const QUrl redirectedUrl = url.resolved(redirectionTarget.toUrl());
		if (QMessageBox::question(this, tr("Redirect"), tr("Redirect to %1 ?").arg(redirectedUrl.toString()), QMessageBox::Yes | QMessageBox::No) == QMessageBox::No)
		{
			QFile::remove(fi.absoluteFilePath());
			return;
		}
		startRequest(redirectedUrl);
		return;
	}

}

void LmmlViewer::httpReadyRead()
{
	QString qSt = QString::fromUtf8(reply->readAll());
	m_textJson = qSt.toUtf8().data();
	eventLoop.exit();
}

void LmmlViewer::slotSaveJSON()
{
	std::string proText = "abcd";
	std::string json = "testJson";
	slotSaveJSON(proText, json);

#ifdef TEST
	slotExit();
#endif
}

void LmmlViewer::slotSaveJSON(std::string activeProfileText, std::string jsonFileName)
{
	if (jsonFileName.length() == 0) {
		QMessageBox::critical(this, tr("Error"), tr("Json file name is empty"));
		return;
	}

	QString dirName = QFileDialog::getExistingDirectory(
		this,
		tr("save directory"),
		"",
		QFileDialog::ShowDirsOnly | QFileDialog::HideNameFilterDetails | QFileDialog::DontUseCustomDirectoryIcons);

	QString saveJsonFileName = dirName + "/" + jsonFileName.c_str() + ".json";
	QString text = activeProfileText.c_str();

	if (!dirName.isEmpty()) {
		QFile file(saveJsonFileName);
		if (!file.open(QIODevice::WriteOnly)) {
			QMessageBox::critical(this, tr("Error"), tr("Could not select directory"));
		}
		else {
			QTextStream stream(&file);
			stream << text;
			stream.flush();
			file.close();
		}
	}

}

void LmmlViewer::slotEnglish()
{
	QString lan = "en";

	QAction *action = qobject_cast<QAction *>(sender());

	QDir dir;
	QString languageFile = dir.currentPath() + "/" + "translations" + "/" + "lmml_" + lan + ".qm";

	QTranslator translator;
	if (!translator.load(languageFile))
	{
		QMessageBox::critical(this, tr("Error"), tr("Could not load qm file"));
		return;
	}
	if (!qApp->installTranslator(&translator))
	{
		QMessageBox::critical(this, tr("Error"), tr("Could not install translator"));
		return;
	}

	retranslateUi(this);
}

void LmmlViewer::slotJapanese()
{
	QString lan = "jp";

	QDir dir;
	QString languageFile = dir.currentPath() + "/" + "translations" + "/" + "lmml_" + lan + ".qm";

	QTranslator translator;
	if (!translator.load(languageFile))
	{
		QMessageBox::critical(this, tr("Error"), tr("Could not load qm file"));
		return;
	}
	if (!qApp->installTranslator(&translator))
	{
		QMessageBox::critical(this, tr("Error"), tr("Could not install translator"));
		return;
	}

	retranslateUi(this);
}

std::map<int, double> LmmlViewer::initialiseSliders()
{

	horizontalSlider_5->setValue(0);
	horizontalSlider_5->setMinimum(0);
	horizontalSlider_5->setMaximum(100);
	horizontalSlider_6->setValue(0);
	horizontalSlider_6->setMinimum(0);
	horizontalSlider_6->setMaximum(100);
	horizontalSlider_7->setValue(0);
	horizontalSlider_7->setMinimum(0);
	horizontalSlider_7->setMaximum(100);
	horizontalSlider_8->setValue(0);
	horizontalSlider_8->setMinimum(0);
	horizontalSlider_8->setMaximum(100);
	horizontalSlider_9->setValue(0);
	horizontalSlider_9->setMinimum(0);
	horizontalSlider_9->setMaximum(100);
	horizontalSlider_10->setValue(0);
	horizontalSlider_10->setMinimum(0);
	horizontalSlider_10->setMaximum(100);
	horizontalSlider_11->setValue(0);
	horizontalSlider_11->setMinimum(0);
	horizontalSlider_11->setMaximum(100);
	horizontalSlider_12->setValue(0);
	horizontalSlider_12->setMinimum(0);
	horizontalSlider_12->setMaximum(100);

	m_sliders[5] = horizontalSlider_5->value();
	m_sliders[6] = horizontalSlider_6->value();
	m_sliders[7] = horizontalSlider_7->value();
	m_sliders[8] = horizontalSlider_8->value();
	m_sliders[9] = horizontalSlider_9->value();
	m_sliders[10] = horizontalSlider_10->value();
	m_sliders[11] = horizontalSlider_11->value();
	m_sliders[12] = horizontalSlider_12->value();

#ifdef TEST
	std::cout << "min,max,value" << std::endl;
	std::cout << "slider5" << std::endl;
	std::cout << horizontalSlider_5->minimum() << "," << horizontalSlider_5->maximum() << "," << horizontalSlider_5->value() << std::endl;
	std::cout << "slider6" << std::endl;
	std::cout << horizontalSlider_6->minimum() << "," << horizontalSlider_6->maximum() << "," << horizontalSlider_6->value() << std::endl;
	std::cout << "slider7" << std::endl;
	std::cout << horizontalSlider_7->minimum() << "," << horizontalSlider_7->maximum() << "," << horizontalSlider_7->value() << std::endl;
	std::cout << "slider8" << std::endl;
	std::cout << horizontalSlider_8->minimum() << "," << horizontalSlider_8->maximum() << "," << horizontalSlider_8->value() << std::endl;
	std::cout << "slider9" << std::endl;
	std::cout << horizontalSlider_9->minimum() << "," << horizontalSlider_9->maximum() << "," << horizontalSlider_9->value() << std::endl;
	std::cout << "slider10" << std::endl;
	std::cout << horizontalSlider_10->minimum() << "," << horizontalSlider_10->maximum() << "," << horizontalSlider_10->value() << std::endl;
	std::cout << "slider11" << std::endl;
	std::cout << horizontalSlider_11->minimum() << "," << horizontalSlider_11->maximum() << "," << horizontalSlider_11->value() << std::endl;
	std::cout << "slider12" << std::endl;
	std::cout << horizontalSlider_12->minimum() << "," << horizontalSlider_12->maximum() << "," << horizontalSlider_12->value() << std::endl;

	injuriesListCallBack("injuryA");
	injuriesListCallBack("injuryB");
	injuriesListCallBack("injuryC");
#endif

	return m_sliders;
}

std::map<int, double> LmmlViewer::sliderCallBack()
{
#ifdef TEST
	std::cout << "sliderCallBack()" << std::endl;
	std::cout << "slider5: " << horizontalSlider_5->value() << std::endl;
	std::cout << "slider6: " << horizontalSlider_6->value() << std::endl;
	std::cout << "slider7: " << horizontalSlider_7->value() << std::endl;
	std::cout << "slider8: " << horizontalSlider_8->value() << std::endl;
	std::cout << "slider9: " << horizontalSlider_9->value() << std::endl;
	std::cout << "slider10: " << horizontalSlider_10->value() << std::endl;
	std::cout << "slider11: " << horizontalSlider_11->value() << std::endl;
	std::cout << "slider12: " << horizontalSlider_12->value() << std::endl;
#endif

	m_sliders[5] = horizontalSlider_5->value();
	m_sliders[6] = horizontalSlider_6->value();
	m_sliders[7] = horizontalSlider_7->value();
	m_sliders[8] = horizontalSlider_8->value();
	m_sliders[9] = horizontalSlider_9->value();
	m_sliders[10] = horizontalSlider_10->value();
	m_sliders[11] = horizontalSlider_11->value();
	m_sliders[12] = horizontalSlider_12->value();

	UpdateRenderer(m_sliders);

	return m_sliders;
}

void LmmlViewer::UpdateRenderer(std::map<int, double> _changedSlider)
{

}

void LmmlViewer::injuriesListCallBack(std::string newInjury)
{
	QListWidgetItem* item = new QListWidgetItem();
	item->setText(QApplication::translate("LmmlViewer", newInjury.c_str(), nullptr));
	listWidget->addItem(item);

#ifdef TEST
	std::string itemName = item->text().toUtf8().constData();
	std::cout << itemName.c_str() << std::endl;
#endif

}

void LmmlViewer::slotExit()
{
#ifdef TEST
	std::cout << "slotExit()" << std::endl;
#endif

	QApplication::exit();

}
