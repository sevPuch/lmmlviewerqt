#Installation for Ubuntu 18.04

Download VTK8.1.2 sources
Download Qt 5.11.X

Build VTK (following the official instal guide) with Qt configuration

cd /PATH_TO_THIS_PROJECT
mkdir build
cd build
cmake ..
make


#Running the program

./LmmlViewer


#Common problem

If VTK build directory isn't found by the compiler, you might need to
set it up manually with 
    $ export VTK_DIR=/path/to/VTK-Release-build/
before building the project.


#Comments

"/Data" folder contains the "FullHead.raw" CT-scan image data and it's header file "FullHead.mhd".
LmmlViewerDriver.cxx contains the main loop of the program.
LmmlViewer.cxx is the class containing all the basic set up of the project.


#VTK building steps
mkdir VTK-build
cd VTK-build
ccmake ../PATH_TO_VTK_SOURCES
    press c to configure
    enable Qt
    write path to Qt directory
    press c to configure
    press g to generate
make -j8