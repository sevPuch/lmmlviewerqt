#include <QApplication>
#include <QSurfaceFormat>
#include "LmmlViewer.h"

#include "vtkGenericOpenGLRenderWindow.h"
#include <vtkNew.h>
#include <vtkPolyDataMapper.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkSphereSource.h>
#include <vtkEventQtSlotConnect.h>
#include <vtkInteractorStyleTrackballActor.h>
#include <vtkFixedPointVolumeRayCastMapper.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkNamedColors.h>
#include <vtkMetaImageReader.h>
#include <vtkPiecewiseFunction.h>
#include <vtkVolumeProperty.h>
#include <vtkCamera.h>
#include <vtkColorTransferFunction.h>

#include <string>

using namespace std;

int main(int argc, char** argv)
{
	// needed to ensure appropriate OpenGL context is created for VTK rendering.
	QSurfaceFormat::setDefaultFormat(QVTKOpenGLWidget::defaultFormat());

	QApplication app(argc, argv);

	LmmlViewer lmmlViewer;
	lmmlViewer.initialiseSliders();

	lmmlViewer.show();

	vtkSmartPointer<vtkRenderer> renderer = lmmlViewer.GetRenderer();

	return app.exec();
}
